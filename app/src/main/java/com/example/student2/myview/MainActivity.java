package com.example.student2.myview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    CircleDiagram diagram;

    float [] data  = {1,2,3,4};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        diagram = (CircleDiagram) findViewById(R.id.circle);


    }

    @Override
    protected void onStart(){
        super.onStart();
        diagram.setDiagramBuildData(data);
    }
}
