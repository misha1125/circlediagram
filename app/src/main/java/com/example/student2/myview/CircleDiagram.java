package com.example.student2.myview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by student2 on 26.11.16.
 */


public class CircleDiagram extends View {


    final int CIRCLE_LENGTH = 100;

    private float [] diagramBuildData = {360};

    int [] colors = {Color.RED,Color.BLACK,Color.BLUE,Color.GRAY,Color.GREEN,Color.CYAN};
    int currentColorId = -1;

    public CircleDiagram(Context context){
        super(context);
    }


    public CircleDiagram(Context context, AttributeSet atr){
        super(context,atr);
    }



    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

        float centerX = canvas.getWidth() / 2;
        float centerY = canvas.getHeight() / 2;

        float width = canvas.getWidth();
        float height = canvas.getHeight();

        float radius = Math.min(width,height)/4;

        RectF diagramRect = new RectF(centerX - radius,centerY - radius ,centerX + radius ,centerY + radius);


        Paint diagramPaint = new Paint();

        diagramPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        diagramPaint.setColor(Color.GREEN);

        float currentAngle = 0;



        for(int i = 0;i<diagramBuildData.length;++i){
            diagramPaint.setColor(GetNextColor());
            canvas.drawArc(diagramRect,currentAngle,diagramBuildData[i],true,diagramPaint);
            currentAngle += diagramBuildData[i];
            Log.d("MA",diagramBuildData[i]+"per");
        }



    }

    public void setDiagramBuildData(float[] diagramBuildData) {
        float sum = arraySum(diagramBuildData);

        float percent = sum / 100.0f;
        float[] percents = new float[diagramBuildData.length];

        for(int i = 0;i<percents.length;++i){
            percents[i] = diagramBuildData[i] / percent * 3.6f;
            Log.d("MA",percents[i]+"");
        }

        this.diagramBuildData = percents;
        invalidate();
    }

    float arraySum(float []ar){
        float sum = 0;

        for(float i:ar){
            sum +=i;
        }
        return sum;
    }

    int GetNextColor(){
        if(++currentColorId >= colors.length)currentColorId = 0;
        return colors[currentColorId];
    }

}
