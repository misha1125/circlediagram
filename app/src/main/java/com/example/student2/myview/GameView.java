package com.example.student2.myview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by student2 on 03.12.16.
 */
public class GameView extends View {

    Vector2 touchPosition = new Vector2(0,0);

    final int CHEES_ITEMS_COUNT = 8;

    int itemSize = 50;

    int maxX;
    int maxY;

    Rect currentTransform;

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameView(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

        Paint blackPaint = new Paint();
        blackPaint.setColor(Color.BLACK);
        Paint whitePaint = new Paint();
        whitePaint.setColor(Color.RED);

        boolean isDrawBlack = false;

        maxY = canvas.getHeight();
        maxX = canvas.getWidth();

        itemSize = Math.min(maxX,maxY) / 8;


        Vector2 begin;

        if(maxX == Math.max(maxX,maxY)){
            begin = new Vector2(maxX / 2 - 4 * itemSize,0);
        }else{
            begin = new Vector2(0,maxY / 2 - 4 * itemSize);
        }

        currentTransform = new Rect(begin.getX(),begin.getY(), begin.getX() + itemSize,begin.getY() + itemSize);
        int itemRadius = itemSize / 2;

        for(int i = 0 ;i< CHEES_ITEMS_COUNT;++i){
            for (int j = 0; j< CHEES_ITEMS_COUNT;++j){

                canvas.drawRect(currentTransform,isDrawBlack?blackPaint:whitePaint);
                isDrawBlack = !isDrawBlack;
                currentTransform.left += itemSize;
                currentTransform.right += itemSize;
            }
            isDrawBlack = !isDrawBlack;
            currentTransform.left = begin.getX();
            currentTransform.right = begin.getX() + itemSize;
            currentTransform.top += itemSize;
            currentTransform.bottom += itemSize;
        }


    }

    void SetTouchCoordinates(Vector2 touch){
        touchPosition = touch;
        if(currentTransform.contains(touchPosition.getX(),touchPosition.getY())){
            invalidate();
        }
    }



    boolean isContains(){
      return touchPosition.getX()<currentTransform.right
          && touchPosition.getX()>currentTransform.left
          && touchPosition.getY()>currentTransform.top
          && touchPosition.getY()<currentTransform.bottom;
    }
}
